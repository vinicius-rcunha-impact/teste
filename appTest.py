import unittest
from app import app


class AppTest(unittest.TestCase):
    def setUp(self):
        self.app = app.test_client()

    def test_print_alunos(self):
        response = self.app.get('/')
        self.assertEqual('<h1>Ac 5 Sendo realizada</h1>' '<h2>alunos:</h2>' '<h3>GUSTAVO GOULART = RA 2201903</h3>' '<h3>VINICIUS BOMFIM =  RA 2201859</h3>''<h3>VINICIUS CUNHA = RA 2201447</h3>' '<h3>VITOR GIRÃO = RA 2201766</h3>''</h1>',
                         response.get_data(as_text=True), "Deu Ruim no test !")

    def test_http_code(self):
        response = self.app.get('/')
        self.assertEqual(200, response.status_code,
                         "Deu ruim no test_http_code!")
        
    def test_upper(self):
        self.assertEqual('foo'.upper(), 'FOO')

    def test_isupper(self):
        self.assertTrue('FOO'.isupper())
        self.assertFalse('Foo'.isupper())
        

if __name__ == "__main__":
    unittest.main()